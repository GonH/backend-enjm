# Backend

## Funcionamiento
El backend funciona con [express](http://expressjs.com/).
Los endpoints se definen en `server.js`. El formato para agregar un endpoint es, arriba del `// app.post` agregar algo con el siguiente formato. 

### Definición del enpoint

```
app.{VERBO}("/{RUTA AL ENPOINT}", jsonParser, (req, res, next) =>
  expressRequestHandler({
    req,
    res,
    next,
    useBody: {true o false},
    handler: {FUNCION QUE MANEJA EL REQUEST},
    reqName: "{FUNCION QUE MANEJA EL REQUESt}"
  })
));
```

Si un valor esta entre llaves significa que es algo que depende de cada endpoint.
- VERBO: El verbo de HTTP, depende de que operación haga el enpoint. La lista de posibles verbos están en [este link](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
- RUTA AL ENPOINT: El nombre de la ruta, lo que va despues del dominio ip al hacer el request para saber a donde hacer el request. Por ejemplo si quisiera obtener un recurso el nombre de la ruta podría ser get-recurso.
- useBody: En este caso es un booleano, si el verbo es `PUT`, `POST` o `PATCH` useBody tiene que ser true. De otro modo tiene que estar en false u omitirse de los parámetros.
- FUNCION QUE MANEJA EL REQUEST: La función importada del modulo correspondiente importada al principio del archivo. En reqName va lo mismo pero entre comillas, se hace por una cuestion de loggeo de errores.

### Declaración del handler

En la carpeta handlers estan los módulos que se encargan de la lógica del backend. Cada archivo deberia manejar la lógica de una entidad como máximo, si un tipo de entidad fuera demasiado compleja, podria dividirse en varios archivos.  
Dentro del archivo las funciones se definen como
```
const {FUNCION QUE MANEJA EL REQUEST} = async (client, { { NOMBRES_DE_PARAMETROS } }) => {
  const collection = client.collection("{NOMBRE DEL MODULO}");

  // LOGICA
}
```
dónde:
- FUNCION QUE MANEJA EL REQUEST: Es el nombre de la función, el mismo que, al final del archivo, se agrega al `module.exports`, y que luego se importa en `server.js`.
- NOMBRES_DE_PARAMETROS: El o los nombres de los parámetros que se envían desde el frontend, ya sea como query params o en el body.
- NOMBRE DEL MODULO: Es importante que un módulo tenga una (o por lo menos una principal) collección (lo que sería como una tabla en la base de datos) asignada. El valor entonces sería el nombre de esa colección para que el conector con MongodB sepa dónde buscar y manejar la información.
- LOGICA: Ahí debería ir lo que se desee hacer, busquedas, inserciones, modificaciones, retorno de datos.


## Setup de base de datos
- [Instalar MongoDB](https://docs.mongodb.com/manual/administration/install-on-linux/)
- En una terminal tipear `mongo`
- Una vez dentro de la consola de mongo, tipear `use enjm-db` para crear la base de datos

## Pasos para levantar el proyecto localmente
1. Instalar nvm, npm y node
2. Hacer el setup de la base de datos (Ver Setup de base de datos)
3. Correr `mongod` y dejar eso en una terminal, debe estar corriendo mientras se quiera acceder a la base de datos
2. Correr `npm install` si no se hizo nunca o se agregó un nuevo módulo
3. Correr `node server.js`

## Links útiles de mongo
- [Instalar mongo local](https://zellwk.com/blog/local-mongodb/)
