const express = require("express");
const mongo = require("mongodb");
const cors = require("cors");
const bodyParser = require("body-parser");
const {
  premiosPonchoSetUp,
  getPremiosPoncho,
  getPremiosPonchoCategories,
  getPremiosPonchoYears,
} = require("./handlers/premiosPoncho");
const {
  getRightNowInfo,
  setUpRightNow,
  updateRightNow,
  deleteFromRightNow,
  gameChange,
} = require("./handlers/rightNow");
const { setUpEnjmInfo, getEnjmInfo } = require("./handlers/enjmInfo");
const {
  insertNewEntry,
  getGalleryPictures,
  getImageById,
  toggleLike,
  addComment,
  getFromEachGallery,
} = require("./handlers/muestras");
const {
  saveGame,
  saveParticipant,
  getSponsors,
  getShops,
  getTeam,
  getGames,
  getEntities,
  getDiscounts,
  saveDiscount,
  getEntityById,
  getPublishers,
  editEntity,
} = require("./handlers/entities");
const {
  saveEvent,
  updateEvent,
  getEvents,
  getGameProposals,
  bulkUpdate
} = require("./handlers/events");
const { getNextUp, updateNextUp } = require("./handlers/adminOnly");
const {
  saveMessage,
  joinMailList,
  getAllMessages,
  markAsRead,
  getAllUnread,
  getAllInList,
  markAsJoined,
  getAllNotJoined,
} = require("./handlers/contact");
const { saveSede, getSedes, updateSede } = require("./handlers/sedes");

const app = express();
const { MongoClient } = mongo;

const whitelist = ["https://www.enjm.com.ar", "https://qa.d19wfbgqf1x5lp.amplifyapp.com", "http://localhost:3000"];
const whitelist2 = ["https://qa.d19wfbgqf1x5lp.amplifyapp.com", "https://www.enjm.com.ar", "http://localhost:3000"];

/* app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
}); */
app.options('*', cors(/*{ origin: whitelist2 }*/))
// app.use(cors());
// app.use(cors({ origin: whitelist2 }));

console.info('precors');

app.use(cors({
  origin: (origin, callback) => {
    console.info('Oh, origin', origin, 'oh whilteslite', whitelist);
    if(!origin) return callback(null, true);
    console.info('Nice origin');
    if(whitelist2.indexOf(origin) === -1){
      console.info('Oh no, cringe');
      const message = "The CORS policy for this origin doesn't allow access from the particular origin.";
      return callback(new Error(message), false);
    }

    console.info('Yeah');
    return callback(null, true);
  }
}));

console.info('postcors');

const jsonParser = bodyParser.json({
  limit: "50mb",
  extended: true,
  parameterLimit: 50000,
});

const MONGO_URL = "mongodb://localhost:27017";
const DB_NAME = "enjm-db";

MongoClient.connect(MONGO_URL, { useUnifiedTopology: true })
  .then((client) => {
    const dbClient = client.db(DB_NAME);
    // app.use(cors());

    const expressRequestHandler = ({
      req: { body, query },
      res,
      next,
      handler,
      useBody,
      reqName,
    }) => {
      const secondParam = useBody ? body : query;

      handler(dbClient, secondParam)
        .then((response) => {
          res.append('Access-Control-Allow-Origin', ['*']);
	  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

          return res.send(response)
        })
        .catch((error) => {
          next(error);
        });
    };

    app.get("/set-up-right-now", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: setUpRightNow,
        reqName: "setUpRightNow",
      })
    );

    app.get("/ahora-mismo", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getRightNowInfo,
        reqName: "getRightNowInfo",
      })
    );

    app.post("/update-right-now", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: updateRightNow,
        useBody: true,
        reqName: "updateRightNow",
      })
    );

    app.put("/game-change", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: gameChange,
        useBody: true,
        reqName: "gameChange",
      })
    );

    app.delete("/delete-chat-from-juegos", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: deleteFromRightNow,
        reqName: "deleteFromRightNow",
      })
    );

    app.get("/get-enjm-info", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getEnjmInfo,
        reqName: "getEnjmInfo",
      })
    );

    app.get("/setup", (req, res, next) => {
      const setUpPromises = [
        premiosPonchoSetUp,
        setUpRightNow,
        setUpEnjmInfo,
      ].map((setUper) => setUper(dbClient));

      Promise.all(setUpPromises).then(() => res.send("nice"));
    });

    app.get("/premios-poncho", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getPremiosPoncho,
        reqName: "getPremiosPoncho",
      })
    );

    app.get("/years", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getPremiosPonchoYears,
        reqName: "getPremiosPonchoYears",
      })
    );

    app.get("/categories", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getPremiosPonchoCategories,
        reqName: "getPremiosPonchoCategories",
      })
    );

    app.post("/new-entry", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: insertNewEntry,
        useBody: true,
        reqName: "insertNewEntry",
      })
    );

    app.get("/get-gallery", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getGalleryPictures,
        reqName: "getGalleryPictures",
      })
    );

    app.get("/get-image-by-id", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getImageById,
        reqName: "getImageById",
      })
    );

    app.put("/toggle-like", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: toggleLike,
        useBody: true,
        reqName: "toggleLike",
      })
    );

    app.put("/add-comment", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: addComment,
        useBody: true,
        reqName: "addComment",
      })
    );

    app.get("/get-from-each-gallery", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getFromEachGallery,
        useBody: true,
        reqName: "getFromEachGallery",
      })
    );

    app.post("/save-game", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: saveGame,
        useBody: true,
        reqName: "saveGame",
      })
    );

    app.get("/get-games", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getGames,
        reqName: "getGames",
      })
    );

    app.put("/update-event", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: updateEvent,
        useBody: true,
        reqName: "updateEvent",
      })
    );

    app.post("/save-event", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: saveEvent,
        useBody: true,
        reqName: "saveEvent",
      })
    );

    app.post("/save-participant", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: saveParticipant,
        useBody: true,
        reqName: "saveParticipant",
      })
    );

    app.get("/get-sponsors", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getSponsors,
        reqName: "getSponsors",
      })
    );

    app.get("/get-shops", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getShops,
        reqName: "getShops",
      })
    );

    app.get("/get-publishers", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getPublishers,
        reqName: "getPublishers",
      })
    );

    app.put("/edit-publisher", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: editEntity,
        useBody: true,
        reqName: "editEntity",
      })
    );

    app.get("/get-team", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getTeam,
        reqName: "getTeam",
      })
    );

    app.get("/get-events", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getEvents,
        reqName: "getEvents",
      })
    );

    app.get("/get-game-proposals", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getGameProposals,
        reqName: "getGameProposals",
      })
    );

    app.get("/get-events", ({ query }, res, next) =>
      expressRequestHandler({
        req: { ...query, current: true },
        res,
        next,
        handler: getEvents,
        reqName: "getEvents",
      })
    );

    app.get("/get-game-proposals", ({ query }, res, next) =>
      expressRequestHandler({
        req: { ...query, current: true },
        res,
        next,
        handler: getGameProposals,
        reqName: "getGameProposals",
      })
    );

    app.get("/get-current-events", ({ query }, res, next) =>
      expressRequestHandler({
        req: { ...query, current: true },
        res,
        next,
        handler: getEvents,
        reqName: "getEvents",
      })
    );

    app.get("/get-current-game-proposals", ({ query }, res, next) =>
      expressRequestHandler({
        req: { ...query, current: true },
        res,
        next,
        handler: getGameProposals,
        reqName: "getGameProposals",
      })
    );

    app.get("/get-old-events", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getEvents,
        reqName: "getEvents",
      })
    );

    app.get("/get-old-game-proposals", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getGameProposals,
        reqName: "getGameProposals",
      })
    );

    app.get("/get-entities", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getEntities,
        reqName: "getEntities",
      })
    );

    app.get("/get-entity", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getEntityById,
        reqName: "getEntityById",
      })
    );

    app.get("/get-discounts", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getDiscounts,
        reqName: "getDiscounts",
      })
    );

    app.post("/save-discount", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: saveDiscount,
        useBody: true,
        reqName: "saveDiscount",
      })
    );

    app.get("/next-up", (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getNextUp,
        reqName: "getNextUp",
      })
    );

    app.put("/next-up", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: updateNextUp,
        useBody: true,
        reqName: "updateNextUp",
      })
    );

    app.post("/save-message", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: saveMessage,
        useBody: true,
        reqName: "saveMessage",
      })
    );

    app.post("/join-list", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: joinMailList,
        useBody: true,
        reqName: "joinMailList",
      })
    );

    app.get("/get-all-messages", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getAllMessages,
        reqName: "getAllMessages",
      })
    );

    app.put("/mark-as-read", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: markAsRead,
        useBody: true,
        reqName: "markAsRead",
      })
    );

    app.get("/get-unread", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getAllUnread,
        reqName: "getAllUnread",
      })
    );

    app.get("/get-in-list", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getAllInList,
        reqName: "getAllInList",
      })
    );

    app.put("/mark-as-joined", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: markAsJoined,
        useBody: true,
        reqName: "markAsJoined",
      })
    );

    app.get("/get-not-joined", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getAllNotJoined,
        reqName: "getAllNotJoined",
      })
    );

    app.get("/sedes", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: getSedes,
        reqName: "getSedes",
      })
    );

    app.post("/sede", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: saveSede,
        useBody: true,
        reqName: "saveSede",
      })
    );

    app.put("/sede", jsonParser, (req, res, next) =>
      expressRequestHandler({
        req,
        res,
        next,
        handler: updateSede,
        useBody: true,
        reqName: "updateSede",
      })
    );
  })
  .catch((error) => {
    console.error("Error at db connect", error);
  });

app.listen(3001, () => console.info("Up!"));

app.get("/", (req, res) => {
  res.send("Meme");
});
