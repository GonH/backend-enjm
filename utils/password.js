const crypto = require("crypto");

const comparePass = (password) => {
  const hashed = crypto.createHash("sha256").update(password).digest("hex");

  if (
    hashed !==
    "16c77cfe8f3c0905049c77bb5018ceb34e8b04c7635cdb290154237481a683d9"
  ) {
    throw new Error("Pass missmatch");
  }
};

module.exports = { comparePass };
