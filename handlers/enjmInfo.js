const { enjms } = require("../enjmDict");

const setUpEnjmInfo = async (client) => {
  const collection = client.collection("enjmInfo");

  const { length } = await collection.find().toArray();

  if (!length) {
    collection.insertMany(enjms);
  }
};

const getEnjmInfo = (client, { id }) => {
  const collection = client.collection("enjmInfo");

  return collection.findOne({ id: Number(id) });
};

module.exports = { setUpEnjmInfo, getEnjmInfo };
