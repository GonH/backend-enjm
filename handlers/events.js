const _ = require("lodash");
const ObjectID = require("mongodb").ObjectID;
const { comparePass } = require("../utils/password");
const fs = require("fs");

const CURRENT_YEAR = new Date().getFullYear();

const filterEvents = (
  collection,
  {
    startHour,
    startMinute,
    endHour,
    endMinute,
    type: filterType,
    day,
    showEverything,
    currentEventType,
    current,
  }
) => {
  const query = { enabled: true };

  if (!showEverything) {
    if (startHour && startMinute)
      query.start = { $gte: `${startHour}:${startMinute}` };
    else if (startHour || startMinute) {
      query[startHour ? "startHour" : "startMinute"] = {
        $gt: startHour || startMinute,
      };
    }

    if (endHour && endMinute) query.end = { $lt: `${endHour}:${endMinute}` };
    else if (endHour || endMinute) {
      query[endHour ? "endHour" : "endMinute"] = { $lte: endHour || endMinute };
    }

    if (day) query.day = day;
  }

  if (filterType) query.type = filterType;
  if (currentEventType) query.currentEventType = currentEventType;

  if (current) query.year = CURRENT_YEAR;
  else query.year = { $lt: CURRENT_YEAR };

  return collection.find(query).toArray();
};

const groupByDayAndType = (toGroup, forbiddenType) => {
  const result = {};
  const typesObj = {};

  toGroup.forEach(({ day, type, ...event }) => {
    if (type !== forbiddenType) {
      let dayObject = result[day];
      if (!dayObject) dayObject = [];

      dayObject.push({ ...event, type });

      result[day] = dayObject;
      typesObj[type] = true;
    }
  });

  const types = Object.keys(typesObj);

  return { result, types };
};

const orderEvents = (
  { start: startA, end: endA },
  { start: startB, end: endB }
) => (`${startA}${endA}` > `${startB}${endB}` ? 1 : -1);

const orderByTime = (toOrder) => {
  const result = {};

  Object.keys(toOrder).forEach((day) => {
    // Object.keys(toOrder[day]).forEach((type) => {
    const orderedDay = toOrder[day] /* [type] */
      .sort(orderEvents);

    // if (!result[day]) result[day] = {};
    result[day] /* [type] */ = orderedDay;
    // });
  });

  return result;
};

const getGameProposals = async (client, query) => {
  const collection = client.collection("events");

  const eventFilter = { ...query, type: "Propuesta Lúdica" };

  const filteredData = await filterEvents(collection, eventFilter);

  const { result, types } = groupByDayAndType(filteredData);
  const orderedByTime = orderByTime(result);

  return { events: orderedByTime, types };
};

const getEvents = async (client, query) => {
  const collection = client.collection("events");

  const filteredData = await filterEvents(collection, query);

  const { result, types } = groupByDayAndType(filteredData, "Propuesta Lúdica");
  const orderedByTime = orderByTime(result);

  return { events: orderedByTime, types };
};

const saveEvent = (
  client,
  {
    name,
    category,
    description,
    password,
    end,
    start,
    day,
    link,
    eventParticipants = [],
    currentEventType,
    inscriptionLink,
  }
) => {
  comparePass(password);
  const collection = client.collection("events");

  const [startHour, startMinute] = start.split(":");
  const [endHour, endMinute] = end.split(":");

  const toInsert = Object.assign({
    name,
    type: category,
    description,
    end,
    start,
    startHour,
    startMinute,
    endHour,
    endMinute,
    day,
    link,
    eventParticipants,
    currentEventType,
    inscriptionLink,
    year: CURRENT_YEAR,
    enabled: true
  });

  return collection.insertOne(toInsert);
};

const updateEvent = async (
  client,
  {
    name,
    category,
    link,
    description,
    startHour,
    startMinute,
    endHour,
    endMinute,
    start,
    end,
    day,
    eventParticipants,
    currentEventType,
    inscriptionLink,
    id,
    year,
  }
) => {
  const collection = client.collection("events");

  const { _id } = await collection.findOne({ _id: ObjectID(id) });
  const query = { enabled: true };

  if (name) query.name = name;
  if (category) query.category = category;
  if (link) query.link = link;
  if (description) query.description = description;
  if (startHour) query.startHour = startHour;
  if (startMinute) query.startMinute = startMinute;
  if (endHour) query.endHour = endHour;
  if (endMinute) query.endMinute = endMinute;
  if (start) {
    query.start = start;
    const [sh, sm] = start.split(":");
    query.startHour = sh;
    query.startMinute = sm;
  }
  if (end) {
    query.end = end;
    const [eh, em] = end.split(":");
    query.endHour = eh;
    query.endMinute = em;
  }
  if (day) query.day = day;
  if (eventParticipants) query.eventParticipants = eventParticipants;
  if (currentEventType) query.currentEventType = currentEventType;
  if (inscriptionLink) query.inscriptionLink = inscriptionLink;
  if (year) query.year = year;

  return collection.updateOne({ _id }, { $set: query });
};

const transformDay = (day) => {
  const dayDict = {
    "1.Jueves": 4,
    "2.Viernes": 5,
    "3.Sábado": 6,
    "4.Domingo": 7,
  };

  return dayDict[day];
};

const transformStart = (hour) => {
  const hourDict = {
    "0.por la mañana": "Por la mañana",
    "0.todo el día": "Todo el día",
    "por la tarde": "Por la tarde",
  };

  const fromDict = hourDict[hour];

  if (fromDict) return { start: fromDict };

  const [startHour, startMinute] = hour.split(":");

  return { start: hour, startHour, startMinute };
};

const identity = (value) => value;

const transformParticipants = (participants) => participants.split(",");

const parseFormat = (ogFormat) => ogFormat || "Virtual";

const parsePlace = (place) => place === "x" ? "" : place;

const colToField = [
  { name: "day", transformation: transformDay },
  { transformation: transformStart },
  { name: "type", transformation: identity }, // type
  { name: "format", transformation: parseFormat }, // format
  { name: "sede", transformation: identity }, // sede
  { name: "place", transformation: parsePlace }, // place
  { name: "name", transformation: identity }, // name
  { name: "eventParticipants", transformation: transformParticipants },
  { name: "description", transformation: identity }, // description,
];

const parseRow = (row, index) => {
  if (!index) return {};
  const cells = row.split("|");

  const rowInfo = {};

  cells.forEach((cell, index) => {
    if (index < colToField.length) {
      const { name, transformation } = colToField[index];

      const transformed = transformation(cell);
      if (name) rowInfo[name] = transformed;
      else {
        Object.keys(transformed).map((key) => {
          rowInfo[key] = transformed[key];
        });
      }
    }
  });

  return rowInfo;
};

const insertIfDoesNotExist = async (event, collection) => {
  const { name, day, ...info } = event;

  if (!name || !day) return;

  const foundEvent = await collection.findOne({ name, day });

  if (foundEvent) {
    await collection.updateOne({ name, day }, { $set: event });
    return;
  }

  const toInsert = {
    name,
    day,
    currentEventType: "Público General",
    year: CURRENT_YEAR,
    ...info
  };

  return collection.insertOne(toInsert);
};

const insertAll = async (events, client) => {
  const collection = client.collection("events");

  for (let i = 0; i < events.length; i++) await insertIfDoesNotExist(events[i], collection);
}

const bulkUpdate = async (client) => {
  const theMeme = (error, data) => {
    if (error) return console.error(error);

    const allRows = data.split("\n");

    const parsedRows = allRows.map(parseRow);

    console.info('the memes', parsedRows);
    insertAll(parsedRows, client);
  };

  await fs.readFile("./handlers/events2.csv", "utf8", theMeme);
};

module.exports = {
  getEvents,
  saveEvent,
  updateEvent,
  getGameProposals,
  bulkUpdate,
};
