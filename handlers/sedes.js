const ObjectID = require("mongodb").ObjectID;
const { comparePass } = require("../utils/password");

const saveSede = (
  client,
  { password, town, link, days, place, placeLink, timeframe, image, organizers = [] }
) => {
  comparePass(password);

  const collection = client.collection("sedes");

  const toInsert = Object.assign({
    town,
    link,
    days,
    place,
    placeLink,
    timeframe,
    image,
    organizers,
  });

  return collection.insertOne(toInsert);
};

const updateSede = (
  client,
  requestBody
) => {
  const { password } = requestBody;
  comparePass(password);

  const collection = client.collection("sedes");

  const set = {};

  const updateField = (fieldName) => {
    if (requestBody[fieldName]) set[fieldName] = requestBody[fieldName]
  };

  updateField('town');
  updateField('link');
  updateField('days');
  updateField('place');
  updateField('placeLink');
  updateField('timeframe');
  updateField('image');
  updateField('organizers');

  return collection.updateOne(
    { _id: ObjectID(requestBody.id) },
    { $set: set }
  );
};

const getSedes = async (client) => {
  const collection = client.collection("sedes");

  return collection.find({}).toArray();
};

module.exports = {
  saveSede,
  getSedes,
  updateSede
};
