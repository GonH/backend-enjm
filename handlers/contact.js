const ObjectID = require("mongodb").ObjectID;
const { comparePass } = require("../utils/password");

const saveMessage = (client, { email, name, topic, message }) => {
  const collection = client.collection("message");

  return collection.insertOne({ email, name, topic, message, read: false });
};

const joinMailList = (client, { email }) => {
  const collection = client.collection("mailList");

  return collection.insertOne({ email, joined: false });
};

const getAllMessages = (client, { password }) => {
  comparePass(password);

  const collection = client.collection("message");

  return collection.find().toArray();
};

const markAsRead = (client, { id, password }) => {
  comparePass(password);

  const collection = client.collection("message");

  return collection.updateOne({ _id: ObjectID(id) }, { $set: { read: true } });
};

const markAsJoined = async (client, { idList, password }) => {
  comparePass(password);

  const collection = client.collection("mailList");

  const mongoIdList = idList.map((id) => ObjectID(id));

  return collection.updateMany(
    { _id: { $in: mongoIdList } },
    { $set: { joined: true } }
  );
};

const getAllUnread = (client, { password }) => {
  comparePass(password);

  const collection = client.collection("message");

  return collection.find({ read: false }).toArray();
};

const getAllInList = async (client, { password }) => {
  comparePass(password);

  const collection = client.collection("mailList");

  return collection.find().toArray();
};

const getAllNotJoined = async (client, { password }) => {
  comparePass(password);

  const collection = client.collection("mailList");

  return collection.find({ joined: false }).toArray();
};

module.exports = {
  saveMessage,
  joinMailList,
  getAllMessages,
  markAsRead,
  getAllUnread,
  getAllInList,
  markAsJoined,
  getAllNotJoined,
};
