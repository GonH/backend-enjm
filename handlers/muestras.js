const ObjectID = require("mongodb").ObjectID;

const insertNewEntry = async (client, { galleryType, ...imageBody }) => {
  if (galleryType === "Competencia_de_Arte") return;

  const collection = client.collection(galleryType);

  const { insertedId } = await collection.insertOne({
    comments: [],
    likes: [],
    ...imageBody,
  });

  return insertedId;
};

const getGalleryPictures = async (client, { galleryType, ids }) => {
  const collection = client.collection(galleryType);
  const SAMPLE_SIZE = 4;

  const aggregation = [];

  if (ids) {
    const idsToAvoidList = ids.split("@").map((id) => ObjectID(id));

    aggregation.push({ $match: { _id: { $nin: idsToAvoidList } } });
  }

  aggregation.push({ $sample: { size: SAMPLE_SIZE } });

  return collection.aggregate(aggregation).limit(4).toArray();
};

const getImageById = async (client, { galleryType, id, browserId }) => {
  const collection = client.collection(galleryType);

  const imageById = await collection.findOne({ _id: ObjectID(id) });

  const { likes, ...rest } = imageById;

  return {
    likes: likes ? likes.length : 0,
    isLikedById: likes && likes.includes(browserId),
    ...rest,
  };
};

const toggleLike = async (client, { galleryType, id, browserId }) => {
  const collection = client.collection(galleryType);

  const { _id, likes } = await collection.findOne({ _id: ObjectID(id) });

  const alreadyLiked = likes.includes(browserId);

  const newLikes = alreadyLiked
    ? likes.filter((likingId) => likingId !== browserId)
    : [...likes, browserId];

  await collection.updateOne({ _id }, { $set: { likes: newLikes } });

  return { status: "ok" };
};

const addComment = async (client, { galleryType, id, comment }) => {
  const collection = client.collection(galleryType);

  const { _id, comments } = await collection.findOne({ _id: ObjectID(id) });

  comments.push(comment);

  await collection.updateOne({ _id }, { $set: { comments } });

  return { status: "ok" };
};

const getRandomElementsFromCollection = async (galleryType, client, size) => {
  const collection = client.collection(galleryType);

  const sample = await collection.aggregate([{ $sample: { size } }]).toArray();

  const sampleWithGalleryType = sample.map((image) => ({
    galleryType,
    ...image,
  }));

  return sampleWithGalleryType;
};

const shuffleArray = (array) => {
  const newArray = new Array(array.length);
  let availableIndexes = array.map((_element, index) => index);

  array.forEach((element) => {
    const randomIndex =
      availableIndexes[Math.floor(Math.random() * availableIndexes.length)];

    newArray[randomIndex] = element;
    availableIndexes = availableIndexes.filter(
      (index) => index !== randomIndex
    );
  });

  return newArray;
};

const getFromEachGallery = async (client) => {
  const COLLECTIONS = [
    "Miniaturas_de_Wargames",
    "Competencia_de_Arte",
    "Actividades_de_Escuelas",
    "Arte",
  ];
  const IMAGE_COUNT_PER_COLLECTION = 5;

  const collectionSearchPromises = COLLECTIONS.map((galleryType) =>
    getRandomElementsFromCollection(
      galleryType,
      client,
      IMAGE_COUNT_PER_COLLECTION
    )
  );

  const [
    minisResult,
    escenoResult,
    escuelasResult,
    arteResult,
  ] = await Promise.all(collectionSearchPromises);

  const allImages = [
    ...minisResult,
    ...escenoResult,
    ...escuelasResult,
    ...arteResult,
  ];

  return shuffleArray(allImages);
};

module.exports = {
  insertNewEntry,
  getGalleryPictures,
  getImageById,
  toggleLike,
  addComment,
  getFromEachGallery,
};
