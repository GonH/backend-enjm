const axios = require("axios");
const { ENVIALO_SIMPLE_KEY } = require("../secretos");

const getEnvialoSimpleUrl = ({ moduleName, action }) =>
  `https://app.envialosimple.com/${moduleName}/${action}?APIKey=${ENVIALO_SIMPLE_KEY}&format=json`;

const subscribeToEmailList = ({ email }) => {
  const subscribeUrl = getEnvialoSimpleUrl({
    moduleName: "member",
    action: "edit",
  });

  axios.post(subscribeUrl, { Email: email });
};

module.exports = {
  subscribeToEmailList,
};
