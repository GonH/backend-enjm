const _ = require("lodash");
const { comparePass } = require("../utils/password");

const getRightNowInfo = async (client) => {
  const collection = client.collection("rightNow");

  const [{
    twitchInfo,
    youtubeInfo,
    twitchEnabled,
    youtubeEnabled
  }] = await collection.find().toArray();

  return {
    twitchInfo: twitchEnabled && twitchInfo,
    youtubeInfo: youtubeEnabled && youtubeInfo
  };
};

const updateRightNow = async (client, { name, enabled, value, password }) => {
  comparePass(password);

  const collection = client.collection("rightNow");

  const VALID_NAMES = ["twitch", "youtube"];

  if (!VALID_NAMES.includes(name)) return;

  const [rightNowInfo] = await collection.find().toArray();

  const infoName = `${name}Info`;
  const enabledName = `${name}Enabled`;

  const toUpdate = { [infoName]: value, [enabledName]: enabled };

  await collection.updateOne({ _id: rightNowInfo._id }, { $set: toUpdate });

  return { status: "ok" };
};

const handleGameEnd = ({
  savedGame,
  gameName,
  games,
  channelId,
  channelName,
}) => {
  if (!savedGame)
    return {
      type: "nothing",
    };

  if (gameName === savedGame) {
    delete games[channelId];

    return {
      games,
      type: "stopped",
    };
  }

  return {
    games,
    type: "invalid",
  };
};

const handleGameStart = ({ savedGame, gameName, games, channelId }) => {
  if (savedGame === gameName)
    return {
      type: "same",
    };

  games[channelId] = gameName;

  return {
    games,
    type: savedGame ? "changed" : "started",
  };
};

rightNowGamesUpdateds = {
  terminar: handleGameEnd,
  empezar: handleGameStart,
};

const gameChange = async (
  client,
  { channelId, command, channelName, gameName }
) => {
  const collection = client.collection("rightNow");

  const rightNowInfo = await collection.findOne();

  const { juegos, _id } = rightNowInfo;

  const savedGame = juegos[channelId];

  const { type, games } = rightNowGamesUpdateds[command]({
    savedGame,
    gameName,
    games: juegos,
    channelId,
    channelName,
  });

  rightNowInfo.juegos = games;

  await collection.updateOne({ _id }, { $set: { juegos } });

  console.info("truku resp", { type, savedGame: savedGame || gameName });

  return { type, savedGame: savedGame || gameName };
};

const deleteFromRightNow = async (client, { chatId }) => {
  const collection = client.collection("rightNow");

  const [rightNowInfo] = await collection.find().toArray();

  const { juegos, _id } = rightNowInfo;

  delete juegos[chatId];

  rightNowInfo.juegos = juegos;

  await collection.updateOne({ _id }, { $set: rightNowInfo });

  return { status: "ok" };
};

const setUpRightNow = async (client) => {
  const collection = client.collection("rightNow");

  const { length } = await collection.find().toArray();

  if (!length) {
    collection.insertOne({
      juegos: {},
      twitchInfo: { title: "what is being played?", src: "" },
      youtubeInfo: {
        title: "what is being played?",
        src: "",
      },
    });
  }
};

module.exports = {
  getRightNowInfo,
  setUpRightNow,
  updateRightNow,
  deleteFromRightNow,
  gameChange,
};
