const _ = require("lodash");
const { comparePass } = require("../utils/password");

const updateNextUp = async (
  client,
  { password, events: rawEvents, isYoutube }
) => {
  comparePass(password);

  const events = rawEvents.map(({ value }) => value);

  const collection = client.collection("nextUp");

  const { _id } = await collection.findOne({});

  const id = Math.random() * 10 ** 17;

  return collection.updateOne({ _id }, { $set: { events, isYoutube, id } });
};

const getNextUp = (client) => {
  const collection = client.collection("nextUp");

  return collection.findOne({});
};

module.exports = { getNextUp, updateNextUp };
