const getInfoFromQuery = (query) => {
  if (!query.length) return {};

  const [queryName, queryInfo] = query.split("=");

  const infoValues = queryInfo.split("-");

  return { queryName, infoValues };
};

const handleYearsFilter = (yearsArray, allInfo) => {
  const filteredInfo = {};

  Object.keys(allInfo).forEach((year) => {
    if (yearsArray.includes(`${year}`)) {
      filteredInfo[year] = allInfo[year];
    }
  });

  return filteredInfo;
};

const handleCategoriesFilter = (categoriesArray, exsitingInfo) => {
  const filteredInfo = {};

  Object.keys(exsitingInfo).forEach((year) => {
    const yearInfo = exsitingInfo[year];
    const filteredYearInfo = {};

    Object.keys(yearInfo).forEach((category) => {
      if (categoriesArray.includes(category)) {
        filteredYearInfo[category] = yearInfo[category];
      }
    });

    if (Object.keys(filteredYearInfo).length)
      filteredInfo[year] = filteredYearInfo;
  });

  return filteredInfo;
};

const handlePrizesFiltering = (queryInfo, prizeInfo) => {
  let data = Object.assign({}, prizeInfo);

  const [first = "", second = ""] = queryInfo.split("&");
  const {
    queryName: firstQueryName,
    infoValues: firstInfoValues,
  } = getInfoFromQuery(first);
  const {
    queryName: secondQueryName,
    infoValues: secondInfoValues,
  } = getInfoFromQuery(second);

  if (firstQueryName === "years") {
    data = handleYearsFilter(firstInfoValues, data);
  }

  const firstIsCat = firstQueryName === "categories";
  const secondIsCat = secondQueryName === "categories";

  if (firstIsCat || secondIsCat) {
    data = handleCategoriesFilter(
      firstIsCat ? firstInfoValues : secondInfoValues,
      data
    );
  }

  return data;
};

const KINMO = {
  name: "Kinmo",
  src:
    "https://i.pinimg.com/originals/d8/67/0a/d8670ade5804cdfe09a69b41359c0f7e.jpg",
  link: "https://www.facebook.com/KinmoPasacronos",
  editorial: "",
  autor: "",
};
const CORONA_DE_HIERRO = {
  name: "Corona de Hierro",
  src:
    "https://www.eldragonazul.com.ar/wp-content/uploads/2017/03/Tapa-Corona.jpg",
  link: "https://www.eldragonazul.com.ar/juegos-corona-de-hierro/",
  editorial: "El dragón azul",
  autor: "",
};
const COMBATE_DE_SAN_LORENZO = {
  name: "Combate de San Lorenzo",
  src:
    "https://juegosdemesaargentinos.files.wordpress.com/2015/04/combate-de-san-lorenzo.jpg?w=1180",
  link: "http://epicajuegos.com.ar/juegos/",
  editorial: "Épica",
  autor: "",
};
const DIXIT = {
  name: "Dixit",
  src:
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.iH3fJq-Gy78ZA497btSilwHaHn%26pid%3DApi&f=1",
  editorial: "",
  autor: "Jean-Louis Roubira",
};
const SOY_JUGANDO_A_SER = {
  name: "Soy Jugando a Ser",
  link: "https://www.facebook.com/SoyJugandoaSer",
  editorial: "",
  autor: "",
};
const CONEJOS_EN_EL_HUERTO = {
  name: "Conejos en el Huerto",
  src:
    "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fenderjuegos.com%2Fmedia%2Fcatalog%2Fproduct%2Fcache%2F1%2Fimage%2F85e4522595efc69f496374d01ef2bf13%2Fc%2Fo%2Fconejos05.jpg&f=1&nofb=1",
  link: "https://ruibalgames.com/conejos-por-todas-partes/",
  editorial: "Ruibal",
  autor: "",
};
const CULTIVOS_MUTANTES = {
  name: "Cultivos Mutantes",
  src: "https://www.eldragonazul.com.ar/wp-content/uploads/2020/06/header5.jpg",
  link: "https://www.eldragonazul.com.ar/juegos-cultivos-mutantes/",
  editorial: "El dragón azul",
  autor: "",
};
const SUCESOS_ARGENTINOS = {
  name: "Sucesos Argentinos",
  src: "https://lajugandera.com/wp-content/uploads/2020/04/sucesos-1.jpg",
  editorial: "",
  autor: "",
};
const CARCASSONE = {
  name: "Carcassone",
  src:
    "https://www.misifu.es/wp-content/uploads/2018/11/carcassonne-juego-de-mesa-min.png",
  link: "https://aaludica.com.ar/",
  editorial: "",
  autor: "Klaus-Jürgen Wrede",
};
const NACIO_POPULAR = {
  name: "Nació Popular",
  src:
    "https://d26lpennugtm8s.cloudfront.net/stores/579/420/products/img_48071-a3db0a7c2a78eed26e15663097986581-320-01-2364becf962c8e3ffe15980277672132-1024-1024.jpg",
  link: "https://www.facebook.com/editorial.lajugandera",
  editorial: "La Jugandera",
  autor: "",
};
const CAMARERO = {
  name: "Camarero",
  src:
    "https://static.mercadoshops.com/el-camarero-juego-de-mesa-8-anos_iZ1053241407XvZgrandeXpZ1XfZ75501715-760704306-1XsZ75501715xIM.jpg",
  link: "https://maldon.com.ar/blog/projects/el-camarero/",
  editorial: "Maldón",
  autor: "",
};
const LUNES = {
  name: "Lunes",
  src:
    "https://static.mercadoshops.com/lunes-super-noob-juego-de-mesa-solitario-nunez_iZ1071414570XvZmediumXpZ1XfZ77637213-792733448-1XsZ77637213xIM.jpg",
  link: "http://supernoobgames.com/lunes/",
  editorial: "Super Noob Games",
  autor: "",
};
const FAST_FOOD = {
  name: "Fast Food!",
  src: "http://juegosdemesa.com.ar/fotos/fastfood1.jpg",
  link: "http://juegosdemesa.com.ar/juegos/#fastfood",
  editorial: "Juegos de Mesa",
  autor: "",
};
const MAGIC_MAZE = {
  name: "Magic Maze",
  src:
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.YrJ1jn8mr-tUJoK8Gqz4FgHaHa%26pid%3DApi&f=1",
  editorial: "Sit Down!",
  autor: "Kasper Lapp",
};

exports.handler = async (event) => {
  let premios = {
    2017: {
      Infantil: KINMO,
      Familiar: KINMO,
      Adultes: CORONA_DE_HIERRO,
      Introductorio: KINMO,
      Identidad: COMBATE_DE_SAN_LORENZO,
      Extranjero: DIXIT,
    },
    2018: {
      Infantil: SOY_JUGANDO_A_SER,
      Familiar: CONEJOS_EN_EL_HUERTO,
      Adultes: CONEJOS_EN_EL_HUERTO,
      Introductorio: CULTIVOS_MUTANTES,
      Identidad: SUCESOS_ARGENTINOS,
      Extranjero: CARCASSONE,
    },
    2019: {
      Infantil: NACIO_POPULAR,
      Familiar: CAMARERO,
      Adultes: LUNES,
      Introductorio: FAST_FOOD,
      Identidad: SUCESOS_ARGENTINOS,
      Diversidad: SOY_JUGANDO_A_SER,
      Extranjero: MAGIC_MAZE,
    },
  };

  const { rawQueryString } = event;

  if (rawQueryString) {
    premios = handlePrizesFiltering(rawQueryString, premios);
  }

  const response = {
    statusCode: 200,
    body: JSON.stringify(premios),
  };

  return response;
};
