/* Endpoints */

const getPremiosPoncho = async (client, { years, categories }) => {
  const collection = client.collection("premiosPoncho");

  const query = {};

  if (years && years.length) query.Year = { "$in": years };
  if (categories && categories.length) query.Category = { "$in": categories };

  const rawResult = await collection.find(query).toArray();

  const result = {};

  rawResult.forEach(({ Year, Category, Game }) => {
    if (!result[Year]) result[Year] = {};

    if (!result[Year][Category]) result[Year][Category] = {};

    result[Year][Category] = Game;
  })

  return result;
};

const getPremiosPonchoCategories = async (client) => {
  const collection = client.collection("premiosPonchoCategories");

  const rawCategories = await collection.find().toArray();

  return rawCategories.map(({ Name }) => Name);
};

const getPremiosPonchoYears = async (client) => {
  const collection = client.collection("premiosPonchoYears");

  const rawYears = await collection.find().toArray();

  return rawYears.map(({ Year }) => Year);
};

/* Seccion de setup */

const KINMO = {
  name: "Kinmo",
  src:
    "https://i.pinimg.com/originals/d8/67/0a/d8670ade5804cdfe09a69b41359c0f7e.jpg",
  link: "https://www.facebook.com/KinmoPasacronos",
  editorial: "",
  autor: "",
};
const CORONA_DE_HIERRO = {
  name: "Corona de Hierro",
  src:
    "https://www.eldragonazul.com.ar/wp-content/uploads/2017/03/Tapa-Corona.jpg",
  link: "https://www.eldragonazul.com.ar/juegos-corona-de-hierro/",
  editorial: "El dragón azul",
  autor: "",
};
const COMBATE_DE_SAN_LORENZO = {
  name: "Combate de San Lorenzo",
  src:
    "https://juegosdemesaargentinos.files.wordpress.com/2015/04/combate-de-san-lorenzo.jpg?w=1180",
  link: "http://epicajuegos.com.ar/juegos/",
  editorial: "Épica",
  autor: "",
};
const DIXIT = {
  name: "Dixit",
  src:
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse1.mm.bing.net%2Fth%3Fid%3DOIP.iH3fJq-Gy78ZA497btSilwHaHn%26pid%3DApi&f=1",
  editorial: "",
  autor: "Jean-Louis Roubira",
};
const SOY_JUGANDO_A_SER = {
  name: "Soy Jugando a Ser",
  link: "https://www.facebook.com/SoyJugandoaSer",
  editorial: "",
  autor: "",
};
const CONEJOS_EN_EL_HUERTO = {
  name: "Conejos en el Huerto",
  src:
    "https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fenderjuegos.com%2Fmedia%2Fcatalog%2Fproduct%2Fcache%2F1%2Fimage%2F85e4522595efc69f496374d01ef2bf13%2Fc%2Fo%2Fconejos05.jpg&f=1&nofb=1",
  link: "https://ruibalgames.com/conejos-por-todas-partes/",
  editorial: "Ruibal",
  autor: "",
};
const CULTIVOS_MUTANTES = {
  name: "Cultivos Mutantes",
  src: "https://www.eldragonazul.com.ar/wp-content/uploads/2020/06/header5.jpg",
  link: "https://www.eldragonazul.com.ar/juegos-cultivos-mutantes/",
  editorial: "El dragón azul",
  autor: "",
};
const SUCESOS_ARGENTINOS = {
  name: "Sucesos Argentinos",
  src: "https://lajugandera.com/wp-content/uploads/2020/04/sucesos-1.jpg",
  editorial: "",
  autor: "",
};
const CARCASSONE = {
  name: "Carcassone",
  src:
    "https://www.misifu.es/wp-content/uploads/2018/11/carcassonne-juego-de-mesa-min.png",
  link: "https://aaludica.com.ar/",
  editorial: "",
  autor: "Klaus-Jürgen Wrede",
};
const NACIO_POPULAR = {
  name: "Nació Popular",
  src:
    "https://d26lpennugtm8s.cloudfront.net/stores/579/420/products/img_48071-a3db0a7c2a78eed26e15663097986581-320-01-2364becf962c8e3ffe15980277672132-1024-1024.jpg",
  link: "https://www.facebook.com/editorial.lajugandera",
  editorial: "La Jugandera",
  autor: "",
};
const CAMARERO = {
  name: "Camarero",
  src:
    "https://static.mercadoshops.com/el-camarero-juego-de-mesa-8-anos_iZ1053241407XvZgrandeXpZ1XfZ75501715-760704306-1XsZ75501715xIM.jpg",
  link: "https://maldon.com.ar/blog/projects/el-camarero/",
  editorial: "Maldón",
  autor: "",
};
const LUNES = {
  name: "Lunes",
  src:
    "https://static.mercadoshops.com/lunes-super-noob-juego-de-mesa-solitario-nunez_iZ1071414570XvZmediumXpZ1XfZ77637213-792733448-1XsZ77637213xIM.jpg",
  link: "http://supernoobgames.com/lunes/",
  editorial: "Super Noob Games",
  autor: "",
};
const FAST_FOOD = {
  name: "Fast Food!",
  src: "http://juegosdemesa.com.ar/fotos/fastfood1.jpg",
  link: "http://juegosdemesa.com.ar/juegos/#fastfood",
  editorial: "Juegos de Mesa",
  autor: "",
};
const MAGIC_MAZE = {
  name: "Magic Maze",
  src:
    "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ftse3.mm.bing.net%2Fth%3Fid%3DOIP.YrJ1jn8mr-tUJoK8Gqz4FgHaHa%26pid%3DApi&f=1",
  editorial: "Sit Down!",
  autor: "Kasper Lapp",
};

const insertPremiosPonchoYears = async (client) => {
  const collection = client.collection("premiosPonchoYears");

  const { length } = await collection.find().toArray();

  if (!length) {
    collection.insertMany([{ Year: 2017 }, { Year: 2018 }, { Year: 2019 }]);
  }
};

const insertPremiosPonchoCategories = async (client) => {
  const collection = client.collection("premiosPonchoCategories");

  const { length } = await collection.find().toArray();

  if (!length) {
    collection.insertMany([
      { Name: "Infantil" },
      { Name: "Familiar" },
      { Name: "Adultes" },
      { Name: "Introductorio" },
      { Name: "Identidad" },
      { Name: "Diversidad" },
      { Name: "Extranjero" },
    ]);
  }
};

const insertPremiosPoncho = async (client) => {
  const collection = client.collection("premiosPoncho");

  const { length } = await collection.find().toArray();

  if (!length) {
    collection.insertMany([
      { Year: 2017, Category: "Infantil", Game: KINMO },
      { Year: 2017, Category: "Familiar", Game: KINMO },
      { Year: 2017, Category: "Adultes", Game: CORONA_DE_HIERRO },
      { Year: 2017, Category: "Introductorio", Game: KINMO },
      { Year: 2017, Category: "Identidad", Game: COMBATE_DE_SAN_LORENZO },
      { Year: 2017, Category: "Extranjero", Game: DIXIT },
      { Year: 2018, Category: "Infantil", Game: SOY_JUGANDO_A_SER },
      { Year: 2018, Category: "Familiar", Game: CONEJOS_EN_EL_HUERTO },
      { Year: 2018, Category: "Adultes", Game: CONEJOS_EN_EL_HUERTO },
      { Year: 2018, Category: "Introductorio", Game: CULTIVOS_MUTANTES },
      { Year: 2018, Category: "Identidad", Game: SUCESOS_ARGENTINOS },
      { Year: 2018, Category: "Extranjero", Game: CARCASSONE },
      { Year: 2019, Category: "Infantil", Game: NACIO_POPULAR },
      { Year: 2019, Category: "Familiar", Game: CAMARERO },
      { Year: 2019, Category: "Adultes", Game: LUNES },
      { Year: 2019, Category: "Introductorio", Game: FAST_FOOD },
      { Year: 2019, Category: "Identidad", Game: SUCESOS_ARGENTINOS },
      { Year: 2019, Category: "Diversidad", Game: SOY_JUGANDO_A_SER },
      { Year: 2019, Category: "Extranjero", Game: MAGIC_MAZE },
    ]);
  }
};

const premiosPonchoSetUp = async (client) => {
  await insertPremiosPonchoCategories(client);
  await insertPremiosPonchoYears(client);
  await insertPremiosPoncho(client);
};

module.exports = {
  premiosPonchoSetUp,
  getPremiosPoncho,
  getPremiosPonchoCategories,
  getPremiosPonchoYears,
};
