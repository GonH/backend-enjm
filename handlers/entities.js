const ObjectID = require("mongodb").ObjectID;
const { comparePass } = require("../utils/password");

const saveGame = (
  client,
  {
    name,
    image,
    category,
    description,
    minPlayerNumber,
    maxPlayerNumber,
    minPlayTime,
    maxPlayTime,
    publisher,
    platform,
    password,
  }
) => {
  comparePass(password);
  const collection = client.collection("games");

  const toInsert = {
    name,
    image,
    category,
    description,
    minPlayerNumber,
    maxPlayerNumber,
    minPlayTime,
    maxPlayTime,
    publisher,
    platform: Object.keys(platform),
  };

  return collection.insertOne(toInsert);
};

const saveParticipant = (
  client,
  { name, image, category, link, tier, password, publisherType }
) => {
  console.info({ name, image, category, link, tier, password, publisherType });
  comparePass(password);
  const collection = client.collection("participants");

  const toInsert = Object.assign({ name, image, category, link, tier });

  if (category === "sponsor") toInsert.publisherType = publisherType;

  return collection.insertOne(toInsert);
};

const getTeam = async (client) => {
  const collection = client.collection("participants");

  return collection.find({ disabled: false, category: "team" }).toArray();
};

const getShops = (client) => {
  const collection = client.collection("participants");

  return collection.find({ disabled: false, category: "shop" }).toArray();
};

const getSponsors = (client) => {
  const collection = client.collection("participants");

  return collection
    .find({ disabled: false, category: { $in: ["sponsor", "shop"] } })
    .toArray();
};

const getPublishers = (client, { publisherType }) => {
  const collection = client.collection("participants");

  const query = { category: "publisher", disabled: false };

  if (publisherType) query.publisherType = publisherType;

  return collection.find(query).toArray();
};

const getShowNowDiscounts = (allDiscounts) => {
  const [currentDate, currentTime] = new Date().toISOString().split("T");
  const [, , day] = currentDate.split("-");
  const hourAndMin = currentTime.slice(0, 5);

  return allDiscounts.filter(
    ({ from, to, activeDays }) =>
      hourAndMin > from && hourAndMin < to && activeDays && activeDays.includes(Number(day))
  );
};

const getDiscounts = async (client) => {
  const collection = client.collection("discount");

  const allDiscounts = await collection.find({ disabled: false }).toArray();

  const showNowDiscounts = getShowNowDiscounts(allDiscounts);

  if (showNowDiscounts.length) return showNowDiscounts;

  const constantDiscounts = allDiscounts.filter(({ from, to, activeDays }) => 
    !from && !to && !(activeDays && activeDays.length)
  )

  return constantDiscounts;
};

const saveDiscount = async (
  client,
  { image, shopId, disabled, password, from, to, activeDays }
) => {
  comparePass(password);

  const shopCollection = client.collection("participants");
  const discountCollection = client.collection("discount");

  const { link, name } = await shopCollection.findOne({
    _id: ObjectID(shopId),
  });

  return discountCollection.insertOne({
    image,
    link,
    name,
    disabled,
    from,
    to,
    activeDays,
    disabled: false
  });
};

const getRangeInfo = (min, max, singularString, pluralString) => {
  let result = min || max;
  let plural = min != 1;
  if (min && max) {
    result = `${min} a ${max}`;
    plural = true;
  }

  return `${result} ${singularString}${plural ? pluralString : ""}`;
};

const getGames = async (
  client,
  { gameType, minPlayerNumber, maxPlayerNumber, minPlayTime, maxPlayTime }
) => {
  const collection = client.collection("games");

  const query = {};
  if (gameType) query.category = gameType;
  if (minPlayerNumber) query.minPlayerNumber = { $gte: minPlayerNumber };
  if (maxPlayerNumber) query.maxPlayerNumber = { $lte: maxPlayerNumber };
  if (minPlayTime) query.minPlayTime = { $gte: minPlayTime };
  if (maxPlayTime) query.maxPlayTime = { $lte: maxPlayTime };

  const rawGames = await collection.find(query).toArray();

  const games = rawGames.map(
    ({
      minPlayerNumber,
      maxPlayerNumber,
      minPlayTime,
      maxPlayTime,
      ...rest
    }) => ({
      ...rest,
      playerCount: getRangeInfo(
        minPlayerNumber,
        maxPlayerNumber,
        "jugador",
        "es"
      ),
      playTime: getRangeInfo(minPlayTime, maxPlayTime, "minuto", "s"),
    })
  );

  return games;
};

const categoryByType = {
  game: "category",
  participants: "category",
  event: "type",
};

const getEntities = async (
  client,
  { collection: collectionName, category }
) => {
  const collection = client.collection(collectionName);

  const query = {};
  if (category) query[categoryByType[collectionName]] = category;

  const rawResult = await collection.find(query).toArray();

  const result = rawResult.map(({ name, _id }) => ({ name, _id }));

  return result;
};

const getEntityById = async (client, { collection: collectionName, _id }) => {
  const collection = client.collection(collectionName);

  return collection.findOne({ _id: ObjectID(_id) });
};

const editEntity = async (client, { _id, image, tier, password }) => {
  comparePass(password);

  const collection = client.collection("participants");

  return collection.updateOne(
    { _id: ObjectID(_id) },
    { $set: { image, tier } }
  );
};

module.exports = {
  saveGame,
  saveParticipant,
  getSponsors,
  getShops,
  getTeam,
  getGames,
  getEntities,
  getDiscounts,
  saveDiscount,
  getEntityById,
  getPublishers,
  editEntity,
};
