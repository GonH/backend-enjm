const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const https = require('https');
const fs = require('fs');
const cert = fs.readFileSync('./gonzah_com.crt');
const ca = fs.readFileSync('./gonzah_com.ca-bundle');
const key = fs.readFileSync('./gonzah_com.p7b');

const app = express();

// view engine setup
app.use(express.bodyParser());
app.use(bodyParser);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// app.use(cors());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

/*const whitelist = ["https://www.enjm.com.ar/", "https://qa.d19wfbgqf1x5lp.amplifyapp.com/", "http://localhost:3000/"];

app.use(cors({
  origin: (origin, callback) => {
    console.info('Oh, origin', origin, 'oh whilteslite', whitelist);
    if(!origin) return callback(new Error(message), false);
    console.info('Nice origin');
    if(whitelist.indexOf(origin) === -1){
      console.info('Oh no, cringe');
      const message = "The CORS policy for this origin doesn't allow access from the particular origin.";
      return callback(new Error(message), false);
    }

    console.info('Yeah');
    return callback(null, true);
  }
}));*/

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
  res.header("Access-Control-Allow-Origin", "*")
});

const httpsOptions = {
  cert,
  ca,
  key
}

console.error('op', httpsOptions)

const httpsServer = https.createServer(httpsOptions, app);

httpsServer.listen(3001, param => {
 console.info('hey, im the callback', param);
});

module.exports = app;
